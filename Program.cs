﻿using System;
using System.Collections.Generic;

namespace Task4YellowPagesMethod
{
	class Program
	{
		public static List<string> person = new List<string>() { "NICK LENNOX", "DEWALD ELS", "PERNILLE HOFGAARD", "KARI NORDMANN", "OLA NORDMANN" };

		static void Main(string[] args)
		{
			
			bool keepSearching = true;
			while (keepSearching)
			{
				Console.WriteLine("1 - Search \n2 - Quit");
				int answer = Convert.ToInt32(Console.ReadLine());
				switch (answer)
				{
					case 1:
						Console.WriteLine("Search: ");
						string name = Console.ReadLine().ToUpper();
						findPerson(name);

						break;
					case 2:
						Console.WriteLine("Quiting");
						keepSearching = false;
						break;
					default:
						Console.WriteLine("Thats not an option");
						break;
				}
			}

		}

		private static void findPerson(string name)
        {
			foreach (string value in person.FindAll(element => element.Contains(name)))
			{
				Console.WriteLine(value.ToLower());
			}
		}
	}
}
